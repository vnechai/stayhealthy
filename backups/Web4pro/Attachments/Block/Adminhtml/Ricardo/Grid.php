<?php
/**
 * WEB4PRO - Creating profitable online stores
 * 
 * @author WEB4PRO <srepin@corp.web4pro.com.ua>
 * @category  WEB4PRO
 * @package   Web4pro_Attachments
 * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
 * @license   http://www.web4pro.net/license.txt
 */
/**
 * Attachment admin grid block
 *
 * @category    Web4pro
 * @package     Web4pro_Attachments
 * @author      WEB4PRO <srepin@corp.web4pro.com.ua>
 */
class Web4pro_Attachments_Block_Adminhtml_Ricardo_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author WEB4PRO <srepin@corp.web4pro.com.ua>
     */
    public function _toHtml()
    {
        $home = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $atHtml = '<h3>Download all current products in Ricardoshops format?</h3>';
        $atHtml .= "<br><br><a style='text-decoration:none;font-size:14px;color:white' href='".$home."soaper?code=1256789'><div style='width:10%;background-color:#5C5D5C;padding:5px;border-radius:1px;text-align:center;border:1px solid black;float:left;margin-left:10%;margin-right:10%;'>Yes</div></a>";
        $atHtml .= "<a style='text-decoration:none;font-size:14px;color:white' href='".$home."admin'><div style='float:left;width:10%;background-color:#5C5D5C;padding:5px;border-radius:1px;text-align:center;border:1px solid black;'>No</div></a><br>";
        
        return $atHtml;
    }
}
