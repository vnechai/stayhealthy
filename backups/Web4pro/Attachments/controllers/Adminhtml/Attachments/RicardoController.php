<?php
/**
 * WEB4PRO - Creating profitable online stores
 * 
 * @author WEB4PRO <srepin@corp.web4pro.com.ua>
 * @category  WEB4PRO
 * @package   Web4pro_Attachments
 * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
 * @license   http://www.web4pro.net/license.txt
 */
/**
 * Attachment admin controller
 *
 * @category    Web4pro
 * @package     Web4pro_Attachments
 * @author      WEB4PRO <srepin@corp.web4pro.com.ua>
 */
class Web4pro_Attachments_Adminhtml_Attachments_RicardoController extends Web4pro_Attachments_Controller_Adminhtml_Attachments
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('web4pro_attachments')->__('Data API'))
             ->_title(Mage::helper('web4pro_attachments')->__('Ricardoshops'));
        
        $this->_setActiveMenu('web4pro_attachments');
        $contentBlock = $this->getLayout()->createBlock('web4pro_attachments/adminhtml_Ricardo_Grid');
        $this->_addContent($contentBlock);
        
        $this->renderLayout();
    }
      
}
