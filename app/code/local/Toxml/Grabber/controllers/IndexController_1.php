<?php
class Toxml_Grabber_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
        //set store to admin
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        //get home url
        $home = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        
        $csvCategory = array_map('str_getcsv', file('Category.csv'));
        //header("Content-Type: text/plain; charset=utf-8");
        
        //check permissions
	if(!isset($_GET["code"]) && $_GET["code"] !== 1256789){exit();} 
        if(!isset($_GET["info"]) && $_GET["info"] !== 1256789){exit();}  else{$info = $_GET['info'];}
        //get xml file
        $fileurl = $home.'media/attachment/file'.$info;
        $xml = simplexml_load_file($fileurl);
        $json = json_encode($xml);
        $arrx = json_decode($json,TRUE);
        
        $prxmlArr = $arrx['product'];
        foreach ($prxmlArr as $key => $prxml ){
            //get info from file
            if (!isset($prxml['disabled']) && $prxml['disabled']){$status = 2;}else{$status = 1;}
            if (isset($prxml['sku'])){$sku = $prxml['sku'];}else{ exit(var_dump($prxml));}
            if (isset($prxml['productname'])){$productname = $prxml['productname'];}else{ exit('One product not have productname');}
            if (isset($prxml['shortdescription'])){$shortdescription = $prxml['shortdescription'];}else{ $shortdescription = "";}
            if (isset($prxml['description'])){$description = $prxml['description'];}else{ $description = "";}
            if (isset($prxml['current_price'])){$price = $prxml['current_price'];}else{ exit('One product not have price');}
            if (isset($prxml['supplier_link'])){$supplier_link = $prxml['supplier_link'];}else{ $supplier_link= "";}
            if (isset($prxml['supplier']) && $prxml['supplier'] ==='Aromalife.ch'){$supplier = 5;}else{ $supplier= 0;}
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (isset($prxml['supplier_productcode'])){$supplier_productcode = $prxml['supplier_productcode'];}else{ $supplier_productcode = "";}
            if (isset($prxml['manufacturer'])){$manufacturer = $prxml['manufacturer'];}else{ $manufacturer = "";}
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

           if (isset($prxml['main_image'])){
                $main_image = $prxml['main_image'];
                $link_image = basename($main_image);
                $way = 'media/catalog/product/fromGrabber/'.$link_image;
                $to_image = '/fromGrabber/'.$link_image;
               if (!file_exists($way)){file_put_contents($way, file_get_contents($main_image));}   

            }else{ exit('One product not have main image');}

            if (isset($prxml['volume'])){$volume = $prxml['volume'];}else{ $volume= "";}
            if (isset($prxml['volume_unit'])){$volume_unit = $prxml['volume_unit'];}else{ $volume_unit = "";}

            //ЗАпитати про це !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (isset($prxml['category_id'])){
                $category_id = $prxml['category_id'];
                
                
            }else{ $category_id = "";    
            }
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            if (isset($prxml['ricardo_category_id'])){$ricardo_category_id = $prxml['ricardo_category_id'];}else{ $ricardo_category_id = "";}
            if (isset($prxml['ricardo_brand'])){$ricardo_brand = $prxml['ricardo_brand'];}else{ $ricardo_brand = "";}
            if (isset($prxml['ricardo_deliveryrange'])){$ricardo_deliveryrange = $prxml['ricardo_deliveryrange'];}else{ $ricardo_deliveryrange = "";}

            if (isset($prxml['deliveryrange'])){$deliveryrange = $prxml['deliveryrange'];}else{ $deliveryrange = "";}

            if (isset($prxml['creationdate'])){$creationdate = $prxml['creationdate'];}else{ $creationdate = "";}
            if (isset($prxml['ricardoshops_prodgender'])){$ricardoshops_prodgender = $prxml['ricardoshops_prodgender'];}else{ $ricardoshops_prodgender = "";}

            
            

            $product = Mage::getModel('catalog/product');
           
            if (!$product->getIdBySku($sku)){
               
                $product
                    ->setStatus($status)//
                    ->setAttributeSetId(4)//
                    ->setTypeId('simple') //
                    ->setCategoryIds(array(4))//
                    ->setWebsiteIds(array(1,2))//
                    ->setMetaTitle($productname)//
                    ->setMetaKeyword($productname)//
                    ->setMetaDescription($productname)//
                    ->setNewsFromDate('06/26/2014') //
                    ->setNewsToDate('06/30/2014')//
                    ->setCountryOfManufacture('AF')//   
                    /*->setCost($price)//   
                    ->setSpecialPrice(00.44)//
                    ->setSpecialFromDate('06/1/2014')//
                    ->setSpecialToDate('06/30/2014') //
                    ->setMsrpEnabled(1)//
                    ->setMsrpDisplayActualPriceType(1)//
                    ->setMsrp($price)*/
                     //->setColor(24)//
                    //->setWeight(4.000)//
                    ->setSku($sku)//  
                    ->setName($productname)//
                    ->setShortDescription($shortdescription)//
                    ->setDescription($description)//
                    ->setPrice($price)//

                    ->setSupplierLnk($supplier_link)
                    ->setSupplier($supplier)
                    ->setSupplierId($supplier_productcode)
                    ->setManufacturer(28)//->setManufacturer($manufacturer)
                        ->setImage($to_image)
                    ->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
                        ->addImageToMediaGallery($way, array('image','thumbnail','small_image'), false, false) //
                    ->setVolume($volume)//
                    ->setVolumeUnit($volume_unit)//
                    ->setCategoryOldId($category_id)
                    ->setRicardoshopsCategory($ricardo_category_id)
                    ->setRicardoshopsBrand($ricardo_brand)
                    ->setRicardoshopsDelivery($ricardo_deliveryrange)
                    ->setDeliveryrange($deliveryrange)
                    ->setCreatedAt($creationdate)
                    ->setRicardoshopsProgender($ricardoshops_prodgender)

                    ->setTaxClassId(2)//
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //
                    ->setStockData(array(//
                           'use_config_manage_stock' => 0, //'Use config settings' checkbox
                           'manage_stock'=>1, //manage stock
                           'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
                           'max_sale_qty'=>999, //Maximum Qty Allowed in Shopping Cart
                           'is_in_stock' => 1, //Stock Availability
                           'qty' => 100 //qty
                                        )
                                );
                $product->save();
            }else{
                $product->load($product->getIdBySku($sku))
                    ->setWebsiteIds(array(1,2)) //website ID the product is assigned to, as an array
                    ->setAttributeSetId(4)
                    ->setCategoryIds(array(4))//
                    ->setStatus($status)
                    ->setName($productname)
                    ->setShortDescription($shortdescription)
                    ->setDescription($description)
                    ->setPrice($price)
                    ->setSupplierLnk($supplier_link)
                    ->setSupplier($supplier)
                    ->setSupplierId($supplier_productcode)
                    ->setManufacturer($manufacturer)
                    ->setImage($to_image)
                    ->setVolume($volume)
                    ->setVolumeUnit($volume_unit)
                    ->setCategoryOldId($category_id)
                    ->setRicardoshopsCategory($ricardo_category_id)
                    ->setRicardoshopsBrand($ricardo_brand)
                    ->setRicardoshopsDelivery($ricardo_deliveryrange)
                    ->setDeliveryrange($deliveryrange)
                    ->setCreatedAt($creationdate)
                    ->setRicardoshopsProgender($ricardoshops_prodgender)

                    ->setTaxClassId(2)//
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) 
                    ->setStockData(array(
                        'qty'=>100,
                        'is_in_stock'=>1));
            
            $product->save();
            }
        }
       
        echo "<pre>";
       //print_r($optionId);
        echo '</pre>';
        
        /*$url=$home."admin";
        $this->getResponse()->setRedirect($url);*/
    }
}
