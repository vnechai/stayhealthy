<?php
class Toxml_Grabber_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
        //set store to admin
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        //get home url
        $home = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        //check permissions
	if(!isset($_GET["code"]) && $_GET["code"] !== 1256789){exit();} 
        if(!isset($_GET["info"]) && $_GET["info"] !== 1256789){exit();}  else{$info = $_GET['info'];}
        //get all categorys from csv file
        $csvCategory = array_map('str_getcsv', file('CategoryId.csv'));
        header("Content-Type: text/plain; charset=utf-8");
        //get xml file
        $fileurl = $home.'media/attachment/file'.$info;
        $xml = simplexml_load_file($fileurl);
        $json = json_encode($xml);
        $arrx = json_decode($json,TRUE);
        $prxmlArr = $arrx['product'];
        
        //loop all products from file
        foreach ($prxmlArr as $key => $prxml ){
             $product = Mage::getModel('catalog/product');
            
            //get info from xml file
            if (!isset($prxml['disabled']) && $prxml['disabled']){$status = 2;}else{$status = 1;}
            if (isset($prxml['sku'])){$sku = $prxml['sku'];}else{ exit(var_dump($prxml));}
            if (isset($prxml['productname'])){$productname = $prxml['productname'];}else{ exit('One product not have productname');}
            if (isset($prxml['shortdescription'])){$shortdescription = $prxml['shortdescription'];}else{ $shortdescription = "";}
            if (isset($prxml['description'])){$description = $prxml['description'];}else{ $description = "";}
            if (isset($prxml['current_price'])){$price = $prxml['current_price'];}else{ exit('One product not have price');}
            if (isset($prxml['supplier_link'])){$supplier_link = $prxml['supplier_link'];}else{ $supplier_link= "";}
            
                //set supplier by id
            if (isset($prxml['supplier'])){
                $supplier = $prxml['supplier'];
                $attributesSupplier = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter($product->getResource()->getTypeId())
                        ->addFieldToFilter('attribute_code', 'supplier');
                    $attributeS = $attributesSupplier->getFirstItem()->setEntity($product->getResource());
                    $suppliersIds = $attributeS->getSource()->getAllOptions(false);
                    
                    foreach ($suppliersIds as $key => $value){
                        if ($value['label'] === $supplier){ $supplier = $value['value']; break;}
                    }
            }else{ $supplier= 0;}
            
                //set supplier productcode
            if (isset($prxml['supplier_productcode'])){$supplier_productcode = $prxml['supplier_productcode'];}else{ $supplier_productcode = "";}
                //set manufacturer
            if (isset($prxml['manufacturer'])){
                $manufacturer = $prxml['manufacturer'];
                    $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter($product->getResource()->getTypeId())
                        ->addFieldToFilter('attribute_code', 'manufacturer');
                    $attribute = $attributes->getFirstItem()->setEntity($product->getResource());
                    $manufacturers = $attribute->getSource()->getAllOptions(false);
                    foreach ($manufacturers as $key => $value){
                        if ($value['label'] === $manufacturer){ $manufacturer = $value['value']; break;}
                    }
            }else{ $manufacturer = "";}
            
                //set main image
           if (isset($prxml['main_image'])){
                $main_image = $prxml['main_image'];
                $link_image = basename($main_image);
                $way = 'media/catalog/product/fromGrabber/'.$link_image;
                $to_image = '/fromGrabber/'.$link_image;
               if (!file_exists($way)){file_put_contents($way, file_get_contents($main_image));}   
            }else{ exit('One product not have main image');}
                
            if (isset($prxml['volume'])){$volume = $prxml['volume'];}else{ $volume= "";}
            if (isset($prxml['volume_unit'])){$volume_unit = $prxml['volume_unit'];}else{ $volume_unit = "";}

                //Get array categories
            if ($prxml['category_id'] !== ""){
                $newCats = [];
                foreach ($csvCategory as $key =>$value){
                    if ($value[2] == $prxml['category_id']){$newCats[] = $value[1];}
                }
            }else{ $newCats = array(2);}

            if (isset($prxml['ricardo_category_id'])){$ricardo_category_id = $prxml['ricardo_category_id'];}else{ $ricardo_category_id = "";}
            if (isset($prxml['ricardo_brand'])){$ricardo_brand = $prxml['ricardo_brand'];}else{ $ricardo_brand = "";}
            if (isset($prxml['ricardo_deliveryrange'])){$ricardo_deliveryrange = $prxml['ricardo_deliveryrange'];}else{ $ricardo_deliveryrange = "";}
            if (isset($prxml['deliveryrange'])){$deliveryrange = $prxml['deliveryrange'];}else{ $deliveryrange = "";}
            if (isset($prxml['creationdate'])){$creationdate = $prxml['creationdate'];}else{ $creationdate = "";}

                //set array of gender
            if (isset($prxml['ricardo_prod_gender'])){
                $ricardoshops_prodgender = explode(";" , $prxml['ricardo_prod_gender']);
                $attributesGender = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter($product->getResource()->getTypeId())
                        ->addFieldToFilter('attribute_code', 'ricardoshops_prodgender');
                    $attributeG = $attributesGender->getFirstItem()->setEntity($product->getResource());
                    $genderIds = $attributeG->getSource()->getAllOptions(false);
                    $ricardoshops_prodgender_array = [];
                    foreach ($suppliersIds as $key => $value){
                        if ($value['label'] === $ricardoshops_prodgender[0]){ $ricardoshops_prodgender_array[] = $value['value'];}
                         if ($value['label'] === $ricardoshops_prodgender[1]){ $ricardoshops_prodgender_array[] = $value['value'];}
                    }
            }else{ $ricardoshops_prodgender_array = "";}

            if (!$product->getIdBySku($sku)){
             
                $product
                    ->setStatus($status)//
                    ->setAttributeSetId(4)//
                    ->setTypeId('simple') //
                    ->setCategoryIds($newCats)//
                    ->setWebsiteIds(array(1,2))//
                    ->setMetaTitle($productname)//
                    ->setMetaKeyword($productname." ".$prxml['manufacturer']." ".strip_tags($shortdescription)." ".strip_tags($description))//
                    ->setMetaDescription(str_replace("&quot;","",strip_tags($description)))//
                    /*->setNewsFromDate('06/26/2014') //
                    ->setNewsToDate('06/30/2014')//
                    ->setCountryOfManufacture('AF')   */
                    ->setCost(0.00)//   
                    //->setSpecialPrice(00.44)//
                    //->setSpecialFromDate('06/1/2014')//
                    //->setSpecialToDate('06/30/2014') //
                    //->setMsrpEnabled(1)//
                    //->setMsrpDisplayActualPriceType(1)//
                    //->setMsrp($price)*/
                     //->setColor(24)//
                    ->setWeight(0)//
                    ->setSku($sku)//  
                    ->setName($productname)//
                    ->setShortDescription($shortdescription)//
                    ->setDescription($description)//
                    ->setPrice($price)//
                    ->setSupplierLnk($supplier_link)
                    ->setSupplier($supplier)
                    ->setSupplierId($supplier_productcode)
                    ->setManufacturer(28)//->setManufacturer($manufacturer)
                    ->setImage($to_image)
                    ->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
                    ->addImageToMediaGallery($way, array('image','thumbnail','small_image'), false, false) //
                    ->setImageLabel($productname)
                    ->setSmallimageLabel($productname)
                    ->setThumbLabel($productname)
                    ->setVolume($volume)//
                    ->setVolumeUnit($volume_unit)//
                    ->setCategoryOldId($prxml['category_id'])
                    ->setRicardoshopsCategory($ricardo_category_id)
                    ->setRicardoshopsBrand($ricardo_brand)
                    ->setRicardoshopsDelivery($ricardo_deliveryrange)
                    ->setDeliveryrange($deliveryrange)
                    ->setCreatedAt($creationdate)
                    ->setRicardoshopsProgender($ricardoshops_prodgender_array)
                    ->setTaxClassId(2)//
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //
                    ->setStockData(array(//
                           'use_config_manage_stock' => 0, //'Use config settings' checkbox
                           'manage_stock'=>0, //manage stock
                           'is_in_stock' => 1, //Stock Availability
                           'qty' => 999 //qty
                                        )
                                );
                $product->save();
            }else{
                $product->load($product->getIdBySku($sku))
                    ->setWebsiteIds(array(1,2)) //website ID the product is assigned to, as an array
                    ->setMetaTitle($productname)//
                    ->setMetaKeyword($productname." ".$prxml['manufacturer']." ".strip_tags($shortdescription)." ".strip_tags($description))//
                    ->setMetaDescription(str_replace("&quot;","",strip_tags($description)))//
                    ->setAttributeSetId(4)//
                    ->setCategoryIds($newCats)//
                    ->setStatus($status)//
                    ->setName($productname)
                    ->setShortDescription($shortdescription)
                    ->setDescription($description)
                    ->setPrice($price)
                    ->setSupplierLnk($supplier_link)
                    ->setSupplier($supplier)
                    ->setSupplierId($supplier_productcode)
                    ->setManufacturer($manufacturer)
                    ->setImage($to_image)
                    ->setImageLabel($productname)
                    ->setSmallimageLabel($productname)
                    ->setThumbLabel($productname)
                    ->setVolume($volume)
                    ->setVolumeUnit($volume_unit)
                    ->setCategoryOldId($prxml['category_id'])
                    ->setRicardoshopsCategory($ricardo_category_id)
                    ->setRicardoshopsBrand($ricardo_brand)
                    ->setRicardoshopsDelivery($ricardo_deliveryrange)
                    ->setDeliveryrange($deliveryrange)
                    ->setCreatedAt($creationdate)
                    ->setRicardoshopsProgender($ricardoshops_prodgender_array)
                    ->setTypeId('simple') //
                    ->setTaxClassId(2)//
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) 
                    ->setStockData(array(//
                           'use_config_manage_stock' => 0, //'Use config settings' checkbox
                           'manage_stock'=>0, //manage stock
                           'is_in_stock' => 1, //Stock Availability
                           'qty' => 999 //qty
                                        )
                                );
            $product->save();
            }
            $mediaGallery = $product->getMediaGallery();
                //if there are images
                if (isset($mediaGallery['images'])){
                    //loop through the images
                    foreach ($mediaGallery['images'] as $image){
                        //set the first image as the base image
                        Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('image'=>$image['file']), 0);
                        //stop
                        break;
                    }
                }
        }
        
        $attachment = Mage::getModel('web4pro_attachments/attachment')
                ->getCollection()
                ->addFieldToFilter('uploaded_file',$_GET['info']) 
                ->getFirstItem()
                ->setData('status', 1)
                ->save();
               
       
        echo "<pre>"; 
        //print_r($attachment);
        echo '</pre>';
        
        
        $url=$home."/admin/attachments_attachment/";
        $this->getResponse()->setRedirect($url)->sendResponse($url);
   }
}
