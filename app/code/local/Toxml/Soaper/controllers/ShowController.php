<?php
class Toxml_Soaper_ShowController extends Mage_Core_Controller_Front_Action{
    
    public function IndexAction() {
        $home = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $homedir = $home.'/api/soap/?wsdl';
        
        $client = new SoapClient($homedir);
        // If somestuff requires api authentification,then get a session token
        $session = $client->login('Soaper', '123456789');
        $params = array(array(
        'type' =>
          array(
            'in' => 'simple'
               )
          ));
        $resulter = $client->call($session, 'catalog_product.list', $params);
        /*$resultId = $client->call($session, 'catalog_product.info', 994/*$value["product_id"]);
        $resultImage = $client->call($session, 'catalog_product_attribute_media.list', 994);*/
        
        $resultId = Mage::getModel('catalog/product')->load(1000);
        echo "<pre>";
        print_r($resultId);
        echo '</pre>';
        //variables for xml
        
        $refNumber = strip_tags($resultId['sku']);
        $name = strip_tags($resultId['name']);
        $short_description = strip_tags($resultId['short_description']);
        $description_product = strip_tags($resultId['description']);
        if (isset($resultId['ricardoshops_category'])){$category = strip_tags($resultId['ricardoshops_category']);} else{$category = "";}
        if($resultId['special_price'] ==""){$price_value = $resultId['price']; }else{$price_value = $resultId['special_price'];}
        if($resultId['special_price'] ==""){$price_value_origin = ""; }else{$price_value_origin = $resultId['price'];}
        if (isset($resultId['ricardoshops_disabled'])){$disabled = strip_tags($resultId['ricardoshops_disabled']);} else{$disabled = "";}
        if($disabled !== "" && $category !== ""){$stock = 999; }else{$stock = 0;}
        if (isset($resultId['ricardoshops_delivery'])){$shippingInfo = strip_tags($resultId['ricardoshops_delivery']);} else{$shippingInfo = "";}
        $mainImage = $resultId['image'];
        if(isset($resultId['media_gallery']['images'][1]['file'])){$secondaryImg = $home."media/catalog/product".$resultId['media_gallery']['images'][1]['file'];} else{$secondaryImg = "";}
        if (isset($resultId['ricardoshops_brand'])){$brand = strip_tags($resultId['ricardoshops_brand']);} else{$brand = "";}
        $volume =  strip_tags($resultId['volume']);
        $volume_init =  strip_tags($resultId['volume_unit']);
        $as_sex_ladies = strip_tags("as_sex_ladies");
        $as_sex_man = strip_tags("as_sex_man");
        
        print_r($refNumber);
        echo '<br><br>';
        print_r($name);
         echo '<br><br>';
        print_r($short_description);
          echo '<br><br>';
        print_r($description_product);
          echo '<br><br>';
        print_r($category);
          echo '<br><br>';
        print_r($price_value);
         echo '<br><br>';
         print_r($price_value_origin);
          echo '<br><br>';
         print_r($stock);
          echo '<br>main<br>';
         print_r($mainImage);
          echo '<br>sec<br>';
         print_r($secondaryImg);
         echo '<br><br>';
         print_r($brand);
          echo '<br><br>';
          echo '<br><br>';
        print_r($disabled);
         echo '<br><br>';
        print_r($shippingInfo);
         echo '<br><br>';
         
        
        /*
//xml file       
$xml = <<<XML
<product>
    <refNumber>{$refNumber}</refNumber> 
    <name>
        <translation locale="de">{$name}</translation>
        </name>
    <summary>
        <translation locale="de">{$short_description}</translation>
    </summary>
    <description>
        <translation locale="de">{$description_product}</translation>
    </description>
    <categories>
        <category>{$category}</category>
    </categories>
    <price currency="CHF">
        <value>{$price_value}</value>
        <originalPrice>{$price_value_origin}</originalPrice>
    </price>
    <stock>{$stock}</stock>
        <attributes>
            <attribute key="shippingInfo">
                <entry>
                    <value>{$shippingInfo}</value>
                </entry>
            </attribute>
            <attribute key="mainImage">
                <entry>
                    <value>{$home}media/catalog/product{$mainImage}</value>
                </entry>
            </attribute>
            <attribute key="images">
                <entry>
                    <value>{$secondaryImg}</value>
                </entry>
            </attribute>
            <attribute key="brand">
                <entry>
                    <value>{$brand}</value>
                </entry>
            </attribute>
            <attribute key="dwvs_volume">
                <entry>
                    <value>{$volume} {$volume_init}</value>
                </entry>
            </attribute>
            <attribute key="as_sex">
                <entry>
                    <value>{$as_sex_ladies}</value>
                </entry>
                <entry>
                    <value>{$as_sex_man}</value>
                </entry>
            </attribute>
        </attributes>
</product>
XML;

$sxml = new SimpleXMLElement($xml);         

        
       //saving generated xml file
        $sxml->asXML('test.xml');


        // function defination to convert array to xml
       header('Content-disposition: attachment; filename="test.xml"');
       header('Content-type: "text/xml"; charset="utf8"');
       readfile('test.xml');*/

    }
}