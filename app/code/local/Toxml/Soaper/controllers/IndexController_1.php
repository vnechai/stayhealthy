<?php
class Toxml_Soaper_IndexController extends Mage_Core_Controller_Front_Action{
    
    public function IndexAction() {
        
        $homedir = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/api/soap/?wsdl';
        
        $client = new SoapClient($homedir);
        
        // If somestuff requires api authentification,
        // then get a session token
        $session = $client->login('Soaper', '123456789');
        $params = array(array(
        'type' =>
          array(
            'in' => 'simple'
               )
          ));
        $resulter = $client->call($session, 'catalog_product.list', $params);
        $resultId = $client->call($session, 'catalog_product.info', 994/*$value["product_id"]*/);
        $resultAttribute = $client->call($session, 'product_custom_option.list',994);
        //instructions for array
        if($resultId['special_price'] ==""){$price_value = $resultId['price']; }else{$price_value = $resultId['special_price'];}
        if($resultId['special_price'] ==""){$price_value_origin = ""; }else{$price_value_origin = $resultId['price'];}
        if($resultId['status'] == 1){$stock = 999; }else{$stock = 0;}
        
        $result = array(
            "refNumbe" => $resultId['sku'],
            'name' => array(
                'translation' => $resultId['name']
            ),
            'summary' => array(
                'translation' => $resultId['short_description']
            ),
            'description' => array(
                'translation' => $resultId['description']
            ),
            'categories' => array(
                'category' => 'ricardshop_categori_will_add'//$resultId['description']
            ),
            'price' => array(
                'value' => $price_value,
                'originalPrice' => $price_value_origin,
            ),
            'stock' => $stock,
            'attributes' => array(
                
            )
           
        );
        
        // open and load a XML file

        /*foreach ($resulter as $key => $value){
            $result = $client->call($session, 'catalog_product.info', $value["product_id"]);
            
        }*/
        
        //function to xml
        function array_to_xml($student_info, &$xml_student_info) {
            foreach($student_info as $key => $value) {
                if(is_array($value)) {
                    if(!is_numeric($key)){
                        $subnode = $xml_student_info->addChild("$key");
                        array_to_xml($value, $subnode);
                    }
                    else{
                        $subnode = $xml_student_info->addChild("item$key");
                        array_to_xml($value, $subnode);
                    }
                }
                else {
                    $xml_student_info->addChild("$key",htmlspecialchars("$value"));
                }
            }
        }
        
        // initializing or creating array
        $product_info = array($result);

        // creating object of SimpleXMLElement
        $xml_product_info = new SimpleXMLElement("<?xml version=\"1.0\"?><product></product>");

        // function call to convert array to xml
        array_to_xml($product_info,$xml_product_info);
       //saving generated xml file
        
        $xml_product_info->asXML('test.xml');


        // function defination to convert array to xml
       header('Content-disposition: attachment; filename="test.xml"');
       header('Content-type: "text/xml"; charset="utf8"');
       readfile('test.xml');


        // If you don't need the session anymore
        //$client->endSession($session);

    }
}