<?php
class Toxml_Soaper_OneController extends Mage_Core_Controller_Front_Action{
    
    public function IndexAction() {
        $home = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $homedir = $home.'/api/soap/?wsdl';
        if(!isset($timestamp)){$timestamp = 'products_'.date('YmdHi').'_stayhealthy.xml';}
        $client = new SoapClient($homedir);
        // If somestuff requires api authentification,then get a session token
        $session = $client->login('Soaper', '123456789');
        $params = array(array(
        'type' =>
          array(
            'in' => 'simple'
               )
          ));
        $resulter = $client->call($session, 'catalog_product.list', $params);
        /*$resultId = $client->call($session, 'catalog_product.info', 994/*$value["product_id"]);
        $resultImage = $client->call($session, 'catalog_product_attribute_media.list', 994);*/
        
        $resultId = Mage::getModel('catalog/product')->load(1000);
        //variables for xml
        
        $refNumber = htmlspecialchars($resultId['sku']);
        $name = htmlspecialchars($resultId['name']);
        $short_description = htmlspecialchars($resultId['short_description']);
        $description_product = htmlspecialchars($resultId['description']);
        if (isset($resultId['ricardoshops_category'])){$category = htmlspecialchars($resultId['ricardoshops_category']);} else{$category = "";}
        if($resultId['special_price'] ==""){$price_value = $resultId['price']; }else{$price_value = $resultId['special_price'];}
        $price_value = substr_replace($price_value ,"",-2);
        
        if($resultId['special_price'] ==""){$price_value_origin = ""; }else{$price_value_origin = "<originalPrice>".$resultId['price']."</originalPrice>";}
        if (isset($resultId['ricardoshops_disabled'])){$disabled = htmlspecialchars($resultId['ricardoshops_disabled']);} else{$disabled = "";}
        if($disabled !== "" && $category !== 0 && $resultId['status'] !== 1){$stock = 999; }else{$stock = 0;}
        
        if (isset($resultId['ricardoshops_delivery'])){
            $shippingInfo = htmlspecialchars($resultId['ricardoshops_delivery']);
            $shippingInfo = '<attribute key="shippingInfo">
                <entry>
                    <value>'.htmlspecialchars($resultId['ricardoshops_delivery']).'</value>
                </entry>
            </attribute>';
        } else{$shippingInfo = "";}
        
        if ($resultId['image'] !== "no_selection"){
            $mainImage = '<attribute key="mainImage"><entry><value>'.$home.'media/catalog/product'.$resultId['image'].'</value></entry></attribute>';
        } else{$mainImage = "";}
        
        if(count($resultId['media_gallery']['images']) > 1){
            $secondaryImg = '<attribute key="images">';
            $img_array = $resultId['media_gallery']['images'];
            array_shift($img_array);
            foreach($img_array as $key => $value){
             
             $secondaryImg .= "<entry><value>".$home."media/catalog/product".$value['file']."</value></entry>";
            }
            $secondaryImg .= "</attribute>";
        } else{$secondaryImg = "";}
           
        
        if (isset($resultId['ricardoshops_brand'])){
            $brand = '<attribute key="brand">
                <entry>
                    <value>'.htmlspecialchars($resultId['ricardoshops_brand']).'</value>
                </entry>
            </attribute>';
            
        } else{$brand = "";}
        
        $volume =  htmlspecialchars($resultId['volume']);
        $volume_init =  htmlspecialchars($resultId['volume_unit']);
        if($volume !== ""||$volume_init !== ""){
            $volume_text = '<attribute key="dwvs_volume"><entry><value>'.$volume.' '.$volume_init. '</value></entry></attribute>';}
        else {$volume_text = "";}
        
        if (isset($resultId['ricardoshops_prodgender'])){
            $as_sex_ladies = explode("," , $resultId['ricardoshops_prodgender']);
            $as_sex = '<attribute key="as_sex">';
            foreach($as_sex_ladies as $key => $value){
                if($value == 32){$as_sex_text = "as_sex_ladies";}
                if($value == 33){$as_sex_text = "as_sex_men";}
                if($value == 34){$as_sex_text = "as_sex_girls";}
                if($value == 35){$as_sex_text = "as_sex_boys";}
                $as_sex .= "<entry><value>".$as_sex_text."</value></entry>";
            }
            $as_sex .= "</attribute>";
        } else{$as_sex = "";}
        
//xml file       
$xml = <<<XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?><products>
<product>
    <refNumber>{$refNumber}</refNumber> 
    <name>
        <translation locale="de">{$name}</translation>
        </name>
    <summary>
        <translation locale="de">{$short_description}</translation>
    </summary>
    <description>
        <translation locale="de">{$description_product}</translation>
    </description>
    <categories>
        <category>{$category}</category>
    </categories>
    <price currency="CHF">
        <value>{$price_value}</value>
        {$price_value_origin}
    </price>
    <stock>{$stock}</stock>
        <attributes>
            {$shippingInfo}
            {$mainImage} 
            {$secondaryImg}
            {$brand}
            {$volume_text}
            {$as_sex}
        </attributes>
</product>
</products>
XML;

$sxml = new SimpleXMLElement($xml);    
        
       //saving generated xml file
        $sxml->asXML($timestamp);


        // function defination to convert array to xml
       header('Content-disposition: attachment; filename="'.$timestamp.'"');
       header('Content-type: "text/xml"; charset="utf8"');
       readfile($timestamp);
       
       
       /*$strServer = "*****";
        $strServerPort = "****";
        $strServerUsername = "*****";
        $strServerPassword = "*****";
        $csv_filename = "Test_File.csv";

        //connect to server
        $resConnection = ssh2_connect($strServer, $strServerPort);

        if(ssh2_auth_password($resConnection, $strServerUsername, $strServerPassword)){
            //Initialize SFTP subsystem

            echo "connected";
            $resSFTP = ssh2_sftp($resConnection);    

            $resFile = fopen("ssh2.sftp://{$resSFTP}/".$csv_filename, 'w');
            fwrite($resFile, "Testing");
            fclose($resFile);                   

        }else{
            echo "Unable to authenticate on server";
        }
       
       
       
       $resFile = fopen("ssh2.sftp://{$resSFTP}/".$csv_filename, 'w');
        $srcFile = fopen("/home/myusername/".$csv_filename, 'r');
        $writtenBytes = stream_copy_to_stream($srcFile, $resFile);
        fclose($resFile);
        fclose($srcFile);*/
       
       
       
       
       
       unlink($timestamp);

    }
}